package ru.malakhov.tm.dto;

public final class Success extends Result {

    public Success() {
        success = true;
        message = "";
    }

}
