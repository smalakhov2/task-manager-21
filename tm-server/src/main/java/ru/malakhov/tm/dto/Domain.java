package ru.malakhov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "Domain")
public final class Domain implements Serializable {

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private List<User> users = new ArrayList<>();

    @NotNull
    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(@NotNull List<Project> projects) {
        this.projects = projects;
    }

    public @NotNull List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@NotNull List<Task> tasks) {
        this.tasks = tasks;
    }

    public @NotNull List<User> getUsers() {
        return users;
    }

    public void setUsers(@NotNull List<User> users) {
        this.users = users;
    }

}