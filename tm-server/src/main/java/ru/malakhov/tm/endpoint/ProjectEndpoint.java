package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.IProjectEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(final @NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public @NotNull List<Project> findAll(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clear(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project findOneById(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "id", partName = "id") final @Nullable String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project findOneByIndex(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "index", partName = "index") final @Nullable Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project findOneByName(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "name", partName = "name") final @Nullable String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Project removeOneById(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "id", partName = "id") final @Nullable String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project removeOneByIndex(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "index", partName = "index") final @Nullable Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Project removeOneByName(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "name", partName = "name") final @Nullable String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "index", partName = "index") final @Nullable Integer index,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(), index, name, description);
    }

}
