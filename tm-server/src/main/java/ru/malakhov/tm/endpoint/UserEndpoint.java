package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.IUserEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(final @NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public @NotNull User create(
            @WebParam(name = "login", partName = "login") final @Nullable String login,
            @WebParam(name = "password", partName = "password") final @Nullable String password
    ) {
        return serviceLocator.getUserService().create(login, password);
    }

    @Override
    @WebMethod
    public @NotNull String[] profile(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().profile(session.getUserId());
    }

    @Override
    @WebMethod
    public void changeEmail(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "email", partName = "email") final @Nullable String email
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().changeEmail(session.getUserId(), email);
    }

    @Override
    @WebMethod
    public void changePassword(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "oldPassword", partName = "oldPassword") final @Nullable String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") final @Nullable String newPassword
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().changePassword(session.getUserId(), oldPassword , newPassword);
    }

    @Override
    @WebMethod
    public void changeLogin(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "login", partName = "login") final @Nullable String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().changeLogin(session.getUserId(), login);
    }

    @Override
    @WebMethod
    public void changeFirstName(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "name", partName = "name") final @Nullable String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().changeFirstName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void changeMiddleName(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "name", partName = "name") final @Nullable String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().changeMiddleName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void changeLastName(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "name", partName = "name") final @Nullable String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().changeLastName(session.getUserId(), name);
    }
}
