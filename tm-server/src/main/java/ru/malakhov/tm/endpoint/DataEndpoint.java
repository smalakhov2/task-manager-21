package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumeration.Role;
import ru.malakhov.tm.api.endpoint.IDataEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class DataEndpoint  implements IDataEndpoint {

    private IServiceLocator serviceLocator;

    public DataEndpoint() {
    }

    public DataEndpoint(final @NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void base64Clear(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().base64Clear();
    }

    @Override
    @WebMethod
    public void base64Load(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().base64Load();
    }

    @Override
    @WebMethod
    public void base64Save(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().base64Save();
    }

    @Override
    @WebMethod
    public void binary64Clear(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().binary64Clear();
    }

    @Override
    @WebMethod
    public void binary64Load(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().binary64Load();
    }

    @Override
    @WebMethod
    public void binary64Save(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().binary64Save();
    }

    @Override
    @WebMethod
    public void jsonClearFasterxml(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().jsonClearFasterxml();
    }

    @Override
    @WebMethod
    public void jsonLoadFasterxml(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().jsonLoadFasterxml();
    }

    @Override
    @WebMethod
    public void jsonSaveFasterxml(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().jsonSaveFasterxml();
    }

    @Override
    @WebMethod
    public void xmlClearFasterxml(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().xmlClearFasterxml();
    }

    @Override
    @WebMethod
    public void xmlLoadFasterxml(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().xmlLoadFasterxml();
    }

    @Override
    @WebMethod
    public void xmlSaveFasterxml(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().xmlSaveFasterxml();
    }

    @Override
    @WebMethod
    public void jsonClearJaxb(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().jsonClearJaxb();
    }

    @Override
    @WebMethod
    public void jsonLoadJaxb(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().jsonLoadJaxb();
    }

    @Override
    @WebMethod
    public void jsonSaveJaxb(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().jsonSaveJaxb();
    }

    @Override
    @WebMethod
    public void xmlClearJaxb(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().xmlClearJaxb();
    }

    @Override
    @WebMethod
    public void xmlLoadJaxb(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().xmlLoadJaxb();
    }

    @Override
    @WebMethod
    public void xmlSaveJaxb(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().xmlSaveJaxb();
    }

}
