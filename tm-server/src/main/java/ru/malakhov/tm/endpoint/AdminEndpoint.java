package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumeration.Role;
import ru.malakhov.tm.api.endpoint.IAdminEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminEndpoint implements IAdminEndpoint {

    private IServiceLocator serviceLocator;

    public AdminEndpoint() {
    }

    public AdminEndpoint(final @NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public @NotNull User createWithRole(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "login", partName = "login") final @Nullable String login,
            @WebParam(name = "password", partName = "password") final @Nullable String password,
            @WebParam(name = "role", partName = "role") final @Nullable Role role
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "login", partName = "login") final @Nullable String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "login", partName = "login") final @Nullable String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeById(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "id", partName = "id") final @Nullable String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeById(id);
    }

    @Override
    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "login", partName = "login") final @Nullable String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }
}
