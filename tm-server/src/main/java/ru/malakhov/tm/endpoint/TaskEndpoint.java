package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.ITaskEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class TaskEndpoint implements ITaskEndpoint {

    private IServiceLocator serviceLocator;

    public TaskEndpoint() {
    };

    public TaskEndpoint(final @NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public @NotNull List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task findOneTaskById(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "id", partName = "id") final @Nullable String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task findOneTaskByIndex(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "index", partName = "index") final @Nullable Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Task findOneTaskByName(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "name", partName = "name") final @Nullable String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Task removeOneTaskById(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "id", partName = "id") final @Nullable String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task removeOneTaskByIndex(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "index", partName = "index") final @Nullable Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Task removeOneTaskByName(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "name", partName = "name") final @Nullable String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") final @Nullable Session session,
            @WebParam(name = "index", partName = "index") final @Nullable Integer index,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateTaskByIndex(session.getUserId(), index, name, description);
    }
}
