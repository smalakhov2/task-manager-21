package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.ISessionEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.Fail;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.dto.Success;
import ru.malakhov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint implements ISessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint() {
    }

    public SessionEndpoint(final @NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public @Nullable Session open(
            @WebParam(name = "login", partName = "password") final @Nullable String login,
            @WebParam(name = "password", partName = "password") final @Nullable String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @Override
    @WebMethod
    public @NotNull Result close(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result closeAll(
            @WebParam(name = "session", partName = "session") final @Nullable Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().closeAll(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }
}
