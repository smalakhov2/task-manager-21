package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumeration.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    @Nullable
    Session sign(@Nullable Session session);

    @Nullable
    User getUser(@Nullable Session session);

    @Nullable
    String getUserId(@Nullable Session session);

    @Nullable
    List<Session> getListSession(@Nullable Session session);

    void close(@Nullable Session session);

    void closeAll(@Nullable Session session);

    boolean isValid(@Nullable Session session);

    void validate(@Nullable Session session);

    void validate(@Nullable Session session, @Nullable Role role);

    void signOutByLogin(@Nullable String login);

    void signOutByUserId(@Nullable String userId);


}
