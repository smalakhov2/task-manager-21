package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;

import java.util.List;

public interface IProjectEndpoint {

    void createProject(@Nullable Session session, @Nullable String name, @Nullable String description);

    @NotNull
    List<Project> findAll(@Nullable Session session);

    void clear(@Nullable Session session);

    @NotNull
    Project findOneById(@Nullable Session session,@Nullable String id);

    @NotNull
    Project findOneByIndex(@Nullable Session session,@Nullable Integer index);

    @NotNull
    Project findOneByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Project removeOneById(@Nullable Session session, @Nullable String id);

    @NotNull
    Project removeOneByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Project removeOneByName(@Nullable Session session, @Nullable String name);

    void updateProjectById(@Nullable Session session, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateProjectByIndex(@Nullable Session session, @Nullable Integer index, @Nullable String name, @Nullable String description);

}
