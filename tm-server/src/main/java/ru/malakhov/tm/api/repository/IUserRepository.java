package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    List<User> findAll();

    @NotNull
    User add(@NotNull User user);

    @NotNull
    User findById(@NotNull String id);

    @NotNull
    User findByLogin(@NotNull String login);

    void removeUser(@NotNull User user);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

    @NotNull
    String[] profile(@NotNull String id);

    void changeEmail(@NotNull String id, @NotNull String email);

    void changePassword(@NotNull String id, @NotNull String oldPassword, @NotNull String newPassword);

    void changeLogin(@NotNull String id, @NotNull String login);

    void changeFirstName(@NotNull String id, @NotNull String name);

    void changeMiddleName(@NotNull String id, @NotNull String name);

    void changeLastName(@NotNull String id, @NotNull String name);

}
