package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumeration.Role;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    List<User> findAll();

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User findById(@Nullable String id);

    @NotNull
    User findByLogin(@Nullable String login);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    @NotNull
    String[] profile(@Nullable String id);

    void changeEmail(@Nullable String id, @Nullable String email);

    void changePassword(@Nullable String id, @Nullable String oldPassword,  @Nullable String newPassword);

    void changeLogin(@Nullable String id, @Nullable String login);

    void changeFirstName(@Nullable String id, @Nullable String name);

    void changeMiddleName(@Nullable String id, @Nullable String name);

    void changeLastName(@Nullable String id, @Nullable String name);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
