package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;

public interface IUserEndpoint {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    String[] profile(@Nullable Session session);

    void changeEmail(@Nullable Session session, @Nullable String email);

    void changePassword(@Nullable Session session, @Nullable String oldPassword, @Nullable String newPassword);

    void changeLogin(@Nullable Session session, @Nullable String login);

    void changeFirstName(@Nullable Session session, @Nullable String name);

    void changeMiddleName(@Nullable Session session, @Nullable String name);

    void changeLastName(@Nullable Session session, @Nullable String name);

}
