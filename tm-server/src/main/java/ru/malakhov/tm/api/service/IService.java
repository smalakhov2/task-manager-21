package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntity> {

    @NotNull
    List<E> getList();

    void load(@Nullable E... e);

    void load(@Nullable Collection<E> e);

    void clear();

}
