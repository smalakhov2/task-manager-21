package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.entity.Session;

public interface ISessionEndpoint {

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    @NotNull
    Result close(@Nullable Session session);

    @NotNull
    Result closeAll(@Nullable Session session);

}
