package ru.malakhov.tm.api.service;


public interface IDataService {

    void base64Clear() throws Exception;

    void base64Load() throws Exception;

    void base64Save() throws Exception;

    void binary64Clear() throws Exception;

    void binary64Load() throws Exception;

    void binary64Save() throws Exception;

    void jsonClearFasterxml() throws Exception;

    void jsonLoadFasterxml() throws Exception;

    void jsonSaveFasterxml() throws Exception;

    void xmlClearFasterxml() throws Exception;

    void xmlLoadFasterxml() throws Exception;

    void xmlSaveFasterxml() throws Exception;

    void jsonClearJaxb() throws Exception;

    void jsonLoadJaxb() throws Exception;

    void jsonSaveJaxb() throws Exception;

    void xmlClearJaxb() throws Exception;

    void xmlLoadJaxb() throws Exception;

    void xmlSaveJaxb() throws Exception;

}
