package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {


    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Task task);

    void remove(@Nullable String userId, @Nullable Task task);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    @NotNull
    Task findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task findOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task removeOneByName(@Nullable String userId, @Nullable String name);

    void updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}