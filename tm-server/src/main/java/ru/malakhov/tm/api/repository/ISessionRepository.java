package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    List<Session> findAllByUserId(@NotNull String userId);

    @NotNull
    Session findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

    Session findById(@NotNull String id);

    boolean contains(@NotNull String id);

}
