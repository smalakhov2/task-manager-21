package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumeration.Role;

public interface IAdminEndpoint {

    @NotNull
    User createWithRole(@Nullable Session session, @Nullable String login, @Nullable String password, @Nullable Role role);

    void lockUserByLogin(@Nullable Session session, @Nullable String login);

    void unlockUserByLogin(@Nullable Session session, @Nullable String login);

    void removeById(@Nullable Session session, @Nullable String id);

    void removeByLogin(@Nullable Session session, @Nullable String login);

}
