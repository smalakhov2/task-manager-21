package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Session;

public interface IDataEndpoint {

    void base64Clear(@Nullable Session session) throws Exception;

    void base64Load(@Nullable Session session) throws Exception;

    void base64Save(@Nullable Session session) throws Exception;

    void binary64Clear(@Nullable Session session) throws Exception;

    void binary64Load(@Nullable Session session) throws Exception;

    void binary64Save(@Nullable Session session) throws Exception;

    void jsonClearFasterxml(@Nullable Session session) throws Exception;

    void jsonLoadFasterxml(@Nullable Session session) throws Exception;

    void jsonSaveFasterxml(@Nullable Session session) throws Exception;

    void xmlClearFasterxml(@Nullable Session session) throws Exception;

    void xmlLoadFasterxml(@Nullable Session session) throws Exception;

    void xmlSaveFasterxml(@Nullable Session session) throws Exception;

    void jsonClearJaxb(@Nullable Session session) throws Exception;

    void jsonLoadJaxb(@Nullable Session session) throws Exception;

    void jsonSaveJaxb(@Nullable Session session) throws Exception;

    void xmlClearJaxb(@Nullable Session session) throws Exception;

    void xmlLoadJaxb(@Nullable Session session) throws Exception;

    void xmlSaveJaxb(@Nullable Session session) throws Exception;

}
