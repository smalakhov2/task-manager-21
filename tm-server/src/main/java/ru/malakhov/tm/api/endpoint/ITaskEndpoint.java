package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;

import java.util.List;

public interface ITaskEndpoint {

    void createTask(@Nullable Session session, @Nullable String name, @Nullable String description);

    @NotNull
    List<Task> findAllTask(@Nullable Session session);

    void clearTask(@Nullable Session session);

    @NotNull
    Task findOneTaskById(@Nullable Session session, @Nullable String id);

    @NotNull
    Task findOneTaskByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Task findOneTaskByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Task removeOneTaskById(@Nullable Session session, @Nullable String id);

    @NotNull
    Task removeOneTaskByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Task removeOneTaskByName(@Nullable Session session, @Nullable String name);

    void updateTaskById(@Nullable Session session, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateTaskByIndex(@Nullable Session session, @Nullable Integer index, @Nullable String name, @Nullable String description);

}
