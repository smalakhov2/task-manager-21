package ru.malakhov.tm.entity;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumeration.Role;

public final class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Role role = Role.USER;

    private boolean locked = false;

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(final boolean locked) {
        this.locked = locked;
    }

    public User() {
    }

    public User(final @NotNull String login, final @NotNull String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(final @NotNull String login, final @NotNull String passwordHash, final @Nullable String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public @NotNull String getLogin() {
        return login;
    }

    public void setLogin(final @NotNull String login) {
        this.login = login;
    }

    public @NotNull String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(final @NotNull String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public @Nullable String getEmail() {
        return email;
    }

    public void setEmail(final @Nullable String email) {
        this.email = email;
    }

    public @Nullable String getFirstName() {
        return firstName;
    }

    public void setFirstName(final @Nullable String firstName) {
        this.firstName = firstName;
    }

    public @Nullable String getLastName() {
        return lastName;
    }

    public void setLastName(final @Nullable String lastName) {
        this.lastName = lastName;
    }

    public @Nullable String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(final @Nullable String middleName) {
        this.middleName = middleName;
    }

    public @Nullable Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }
}
