package ru.malakhov.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.Domain;

public final class DomainService implements IDomainService {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    public DomainService(final ITaskService taskService,
                         final IProjectService projectService,
                         final IUserService userService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }


    @Override
    public void load(final @Nullable Domain domain) {
        if (domain == null) return;
        taskService.load(domain.getTasks());
        projectService.load(domain.getProjects());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(final @Nullable Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getList());
        domain.setTasks(taskService.getList());
        domain.setUsers(userService.getList());
    }

}
