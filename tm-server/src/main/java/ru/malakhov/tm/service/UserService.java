package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumeration.Role;
import ru.malakhov.tm.exception.empty.*;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.HashUtil;

import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public @NotNull User findById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public @NotNull User findByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public void removeById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.removeById(id);
    }

    @Override
    public void removeByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @Override
    public @NotNull User create(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final @NotNull User user = new User(login, HashUtil.salt(password));
        user.setRole(Role.USER);
        return userRepository.add(user);
    }

    @Override
    public @NotNull User create(final @Nullable String login, final @Nullable String password, final @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final @NotNull User user = create(login,password);
        user.setEmail(email);
        return user;
    }

    @Override
    public @NotNull User create(final @Nullable String login, final @Nullable String password, final @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final @Nullable User user = create(login,password);
        user.setRole(role);
        return user;
    }

    @Override
    public @NotNull String @NotNull [] profile(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        return userRepository.profile(id);
    }

    @Override
    public void changeEmail(@Nullable String id, final @Nullable String email) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.changeEmail(id, email);
    }

    @Override
    public void changePassword(final @Nullable String id, @Nullable String oldPassword, final @Nullable String newPassword) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new EmptyPasswordException();
        userRepository.changePassword(id, oldPassword, newPassword);
    }

    @Override
    public void changeLogin(final @Nullable String id, final @Nullable String login) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.changeLogin(id, login);
    }

    @Override
    public void changeFirstName(final @Nullable String id, final @Nullable String name) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        userRepository.changeFirstName(id, name);
    }

    @Override
    public void changeMiddleName(final @Nullable String id, final @Nullable String name) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        userRepository.changeMiddleName(id, name);
    }

    @Override
    public void changeLastName(final @Nullable String id, final @Nullable String name) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        userRepository.changeLastName(id, name);
    }

    @Override
    public void lockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final @NotNull User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final @NotNull User user = findByLogin(login);
        user.setLocked(false);
    }

}
