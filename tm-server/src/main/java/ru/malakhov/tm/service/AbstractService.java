package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.api.service.IService;
import ru.malakhov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    public AbstractService(final @NotNull IRepository<E> repository) {
        this.repository = repository;
    }


    @Override
    public @NotNull List<E> getList() {
        return repository.getList();
    }

    @Override
    public void load(final @Nullable E... e) {
        if (e == null) return;
        repository.load(e);
    }

    @Override
    public void load(final @Nullable Collection<E> e) {
        if (e == null) return;
        repository.load(e);
    }

    @Override
    public void clear() {
        repository.clear();
    }
}
