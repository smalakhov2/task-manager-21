package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    private @NotNull final String NAME = "/application.properties";

    private @NotNull final Properties properties = new Properties();

    @Override
    public void init() throws Exception {
        final @NotNull InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
        properties.load(inputStream);
    }

    @Override
    public @NotNull String getServerHost() {
        final @NotNull String propertyHost = properties.getProperty("server.host");
        final @NotNull String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Override
    public @NotNull Integer getServerPort() {
        final @NotNull String propertyPort = properties.getProperty("server.port");
        final @NotNull String envPort = System.getProperty("server.port");
        @NotNull String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @Override
    public @NotNull String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @Override
    public @NotNull Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

}
