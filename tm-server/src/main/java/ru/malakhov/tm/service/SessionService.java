package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumeration.Role;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.util.SignatureUtil;

import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    private final ISessionRepository sessionRepository;

    private final IServiceLocator serviceLocator;

    public SessionService(
            final @NotNull ISessionRepository sessionRepository,
            final @NotNull IServiceLocator serviceLocator
    ){
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @Override
    public boolean checkDataAccess(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        final @NotNull User user = serviceLocator.getUserService().findByLogin(login);
        final @Nullable String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public @Nullable Session open(final @Nullable String login, final @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @NotNull User user = serviceLocator.getUserService().findByLogin(login);
        final @NotNull Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.merge(session);
        return sign(session);
    }

    @Override
    public @Nullable Session sign(final @Nullable Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final @NotNull IPropertyService propertyService = serviceLocator.getPropertyService();
        final @NotNull String salt = propertyService.getSessionSalt();
        final @NotNull Integer cycle = propertyService.getSessionCycle();
        final @Nullable String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public @NotNull User getUser(final @Nullable Session session) {
        final @Nullable String userId = getUserId(session);
        return serviceLocator.getUserService().findById(userId);
    }

    @Override
    public @Nullable String getUserId(final @Nullable Session session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public @NotNull List<Session> getListSession(final @Nullable Session session) {
        validate(session);
        return sessionRepository.findAllByUserId(session.getUserId());
    }

    @Override
    public void close(final @Nullable Session session) {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Override
    public void closeAll(final @Nullable Session session) {
        validate(session);
        sessionRepository.clear();
    }

    @Override
    public boolean isValid(final @Nullable Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(final @Nullable Session session) {
        if (session == null) throw new  AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new  AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new  AccessDeniedException();
        if (session.getTimestamp() == null) throw new  AccessDeniedException();
        final @Nullable Session temp = session.clone();
        if (temp == null) throw new  AccessDeniedException();
        final @Nullable String signatureSource = session.getSignature();
        final @Nullable String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new  AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new  AccessDeniedException();

    }

    @Override
    public void validate(final @Nullable Session session, final @Nullable Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final @Nullable String userId = session.getUserId();
        final @Nullable User user = serviceLocator.getUserService().findById(userId);
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void signOutByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) return;
        final @NotNull User user = serviceLocator.getUserService().findByLogin(login);
        final @Nullable String userId = user.getId();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.removeByUserId(userId);
    }

}
