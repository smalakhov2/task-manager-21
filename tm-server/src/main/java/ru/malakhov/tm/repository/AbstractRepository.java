package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements  IRepository<E> {

    @NotNull
    protected final List<E> records = new ArrayList<>();

    public void clear() {
        records.removeAll(records);
    }

    @Override
    public void merge(final @NotNull E e) {
        records.add(e);
    }

    @SafeVarargs
    @Override
    public final void merge(final @NotNull E... es) {
        for (final @NotNull E e: es) merge(e);
    }

    @Override
    public void merge(final @NotNull Collection<E> es) {
        for (final @NotNull E e: es) merge(e);
    }

    @SafeVarargs
    @Override
    public final void load(final @NotNull E... e) {
        clear();
        merge(e);
    }

    @Override
    public void load(final @NotNull Collection<E> e) {
        clear();
        merge(e);
    }

    @Override
    public @NotNull List<E> getList() {
        return new @NotNull ArrayList<>(records);
    }

}
