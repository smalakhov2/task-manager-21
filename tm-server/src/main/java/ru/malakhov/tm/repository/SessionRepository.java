package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.exception.user.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public @NotNull List<Session> findAllByUserId(final @NotNull String userId) {
        final @NotNull List<Session> result = new ArrayList<>();
        for (final @NotNull Session session: records) {
            if (userId.equals(session.getUserId())) result.add(session);
        }
        return result;
    }

    @Override
    public @NotNull Session findByUserId(@NotNull String userId) {
        for (final @NotNull Session session: records) {
            if (userId.equals(session.getUserId())) return session;
        }
        throw new UserNotFoundException();
    }

    @Override
    public void removeByUserId(final @NotNull String userId) {
        final @NotNull Session session = findByUserId(userId);
        records.remove(session);
    }

    @Override
    public Session findById(final @NotNull String id) {
        for (final @NotNull Session session: getList()){
            if (id.equals(session.getId())) return session;
        }
        return null;
    }

    @Override
    public boolean contains(final @NotNull String id) {
        final @Nullable Session session = findById(id);
        return getList().contains(session);
    }

}
