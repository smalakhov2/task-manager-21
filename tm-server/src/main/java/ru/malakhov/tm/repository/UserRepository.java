package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.exception.user.UserNotFoundException;
import ru.malakhov.tm.util.HashUtil;


import java.util.List;


public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public @NotNull List<User> findAll() {
        return records;
    }

    @Override
    public @NotNull User add(final @NotNull User user) {
        records.add(user);
        return user;
    }

    @Override
    public @NotNull User findById(final @NotNull String id) {
        for (final @NotNull User user : records) {
            if (id.equals(user.getId())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public @NotNull User findByLogin(final @NotNull String login) {
        for (final @NotNull User user : records) {
            if (login.equals(user.getLogin())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public void removeUser(final @NotNull User user) {
        records.remove(user);
    }

    @Override
    public void removeById(final @NotNull String id) {
        final @NotNull User user = findById(id);
        removeUser(user);
    }

    @Override
    public void removeByLogin(final @NotNull String login) {
        final @NotNull User user = findByLogin(login);
        removeUser(user);
    }

    @Override
    public @NotNull String @NotNull [] profile(final @NotNull String id) {
        final @NotNull String[] profileInfo = {"First name: " + findById(id).getFirstName(),
                                "Last name: " + findById(id).getLastName(),
                                "Middle name: " + findById(id).getMiddleName(),
                                "Login: " + findById(id).getLogin(),
                                "E-mail: " + findById(id).getEmail(),
                                "Account type: " + findById(id).getRole().getDisplayName()};
        return profileInfo;
    }

    @Override
    public void changeEmail(final @NotNull String id, final @NotNull String email) {
        final @NotNull User user = findById(id);
        user.setEmail(email);
    }

    @Override
    public void changePassword(final @NotNull String id, @NotNull String oldPassword, final @NotNull String newPassword) {
        final @NotNull User user = findById(id);
        if (!HashUtil.salt(oldPassword).equals(user.getPasswordHash())) throw new AccessDeniedException();
        user.setPasswordHash(HashUtil.salt(newPassword));
    }

    @Override
    public void changeLogin(final @NotNull String id, final @NotNull String login) {
        final @NotNull User user = findById(id);
        user.setLogin(login);
    }

    @Override
    public void changeFirstName(final @NotNull String id, final @NotNull String name) {
        final @NotNull User user = findById(id);
        user.setFirstName(name);
    }

    @Override
    public void changeMiddleName(final @NotNull String id, final @NotNull String name) {
        final @NotNull User user = findById(id);
        user.setMiddleName(name);
    }

    @Override
    public void changeLastName(final @NotNull String id, final @NotNull String name) {
        final @NotNull User user = findById(id);
        user.setLastName(name);
    }
}
