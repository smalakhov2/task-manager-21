package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.project.ProjectNotFoundException;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void add(final @NotNull String userId, final @NotNull Project project) {
        project.setUserId(userId);
        records.add(project);
    }

    @Override
    public void remove(final @NotNull String userId, final @NotNull Project project) {
        if (!userId.equals(project.getUserId())) return;
        records.remove(project);
    }

    @Override
    public @NotNull List<Project> findAll(final @NotNull String userId) {
        final @NotNull List<Project> result = new ArrayList<>();
        for (final @NotNull Project project: records){
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        final @NotNull List<Project> projects = findAll(userId);
        this.records.removeAll(projects);
    }

    @Override
    public @NotNull Project findOneById(final @NotNull String userId, @NotNull String id) {
        for (final @NotNull Project project : records) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public @NotNull Project findOneByIndex(final @NotNull String userId, @NotNull Integer index) {
        if (!userId.equals(records.get(index).getUserId())) throw new ProjectNotFoundException();
        return records.get(index);
    }

    @Override
    public @NotNull Project findOneByName(final @NotNull String userId, final @NotNull String name) {
        for (final @NotNull Project project : records) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public @NotNull Project removeOneById(final @NotNull String userId, final @NotNull String id) {
        final @NotNull Project project = findOneById(userId, id);
        records.remove(project);
        return project;
    }

    @Override
    public @NotNull Project removeOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        final @NotNull Project project = findOneByIndex(userId, index);
        remove(userId, project);
        return project;
    }

    @Override
    public @NotNull Project removeOneByName(final @NotNull String userId, final @NotNull String name) {
        final @NotNull Project project = findOneByName(userId, name);
        remove(userId, project);
        return project;
    }

}