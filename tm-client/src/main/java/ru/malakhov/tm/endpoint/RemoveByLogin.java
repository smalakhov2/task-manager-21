package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeByLogin", propOrder = {
    "session",
    "login"
})
public class RemoveByLogin {

    protected Session session;
    protected String login;

    public Session getSession() {
        return session;
    }

    public void setSession(Session value) {
        this.session = value;
    }

    public String getLogin() {
        return login;
    }


    public void setLogin(String value) {
        this.login = value;
    }

}
