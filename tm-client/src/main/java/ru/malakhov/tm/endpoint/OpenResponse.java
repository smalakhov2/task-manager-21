package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "openResponse", propOrder = {
    "_return"
})
public class OpenResponse {

    @XmlElement(name = "return")
    protected Session _return;

    public Session getReturn() {
        return _return;
    }

    public void setReturn(Session value) {
        this._return = value;
    }

}
