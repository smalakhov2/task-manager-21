package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "result", propOrder = {
    "success",
    "message"
})
public class Result {

    protected Boolean success;
    protected String message;

    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean value) {
        this.success = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String value) {
        this.message = value;
    }

}
