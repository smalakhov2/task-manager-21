package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeOneTaskByIndexResponse", propOrder = {
    "_return"
})
public class RemoveOneTaskByIndexResponse {

    @XmlElement(name = "return")
    protected Task _return;

    public Task getReturn() {
        return _return;
    }

    public void setReturn(Task value) {
        this._return = value;
    }

}
