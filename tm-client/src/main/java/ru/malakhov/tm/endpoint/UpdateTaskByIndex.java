package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateTaskByIndex", propOrder = {
    "session",
    "index",
    "name",
    "description"
})
public class UpdateTaskByIndex {

    protected Session session;
    protected Integer index;
    protected String name;
    protected String description;

    public Session getSession() {
        return session;
    }

    public void setSession(Session value) {
        this.session = value;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer value) {
        this.index = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

}
