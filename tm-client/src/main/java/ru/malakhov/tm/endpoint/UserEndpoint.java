package ru.malakhov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://endpoint.tm.malakhov.ru/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/UserEndpoint/profileRequest", output = "http://endpoint.tm.malakhov.ru/UserEndpoint/profileResponse")
    @RequestWrapper(localName = "profile", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.Profile")
    @ResponseWrapper(localName = "profileResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ProfileResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<java.lang.String> profile(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeFirstNameRequest", output = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeFirstNameResponse")
    @RequestWrapper(localName = "changeFirstName", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeFirstName")
    @ResponseWrapper(localName = "changeFirstNameResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeFirstNameResponse")
    public void changeFirstName(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeMiddleNameRequest", output = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeMiddleNameResponse")
    @RequestWrapper(localName = "changeMiddleName", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeMiddleName")
    @ResponseWrapper(localName = "changeMiddleNameResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeMiddleNameResponse")
    public void changeMiddleName(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeEmailRequest", output = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeEmailResponse")
    @RequestWrapper(localName = "changeEmail", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeEmail")
    @ResponseWrapper(localName = "changeEmailResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeEmailResponse")
    public void changeEmail(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeLoginRequest", output = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeLoginResponse")
    @RequestWrapper(localName = "changeLogin", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeLogin")
    @ResponseWrapper(localName = "changeLoginResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeLoginResponse")
    public void changeLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/UserEndpoint/changePasswordRequest", output = "http://endpoint.tm.malakhov.ru/UserEndpoint/changePasswordResponse")
    @RequestWrapper(localName = "changePassword", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangePassword")
    @ResponseWrapper(localName = "changePasswordResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangePasswordResponse")
    public void changePassword(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "oldPassword", targetNamespace = "")
        java.lang.String oldPassword,
        @WebParam(name = "newPassword", targetNamespace = "")
        java.lang.String newPassword
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/UserEndpoint/createRequest", output = "http://endpoint.tm.malakhov.ru/UserEndpoint/createResponse")
    @RequestWrapper(localName = "create", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.Create")
    @ResponseWrapper(localName = "createResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.CreateResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.User create(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeLastNameRequest", output = "http://endpoint.tm.malakhov.ru/UserEndpoint/changeLastNameResponse")
    @RequestWrapper(localName = "changeLastName", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeLastName")
    @ResponseWrapper(localName = "changeLastNameResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ChangeLastNameResponse")
    public void changeLastName(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );
}
