package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeOneByIndex", propOrder = {
    "session",
    "index"
})
public class RemoveOneByIndex {

    protected Session session;
    protected Integer index;

    public Session getSession() {
        return session;
    }

    public void setSession(Session value) {
        this.session = value;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer value) {
        this.index = value;
    }

}
