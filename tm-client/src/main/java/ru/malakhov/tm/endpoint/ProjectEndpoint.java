package ru.malakhov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


@WebService(targetNamespace = "http://endpoint.tm.malakhov.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/findOneByNameRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/findOneByNameResponse")
    @RequestWrapper(localName = "findOneByName", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneByName")
    @ResponseWrapper(localName = "findOneByNameResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Project findOneByName(
        @WebParam(name = "session", targetNamespace = "")
                ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/updateProjectByIdRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/updateProjectByIdResponse")
    @RequestWrapper(localName = "updateProjectById", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.UpdateProjectById")
    @ResponseWrapper(localName = "updateProjectByIdResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.UpdateProjectByIdResponse")
    public void updateProjectById(
        @WebParam(name = "session", targetNamespace = "")
                ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/updateProjectByIndexRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/updateProjectByIndexResponse")
    @RequestWrapper(localName = "updateProjectByIndex", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.UpdateProjectByIndex")
    @ResponseWrapper(localName = "updateProjectByIndexResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.UpdateProjectByIndexResponse")
    public void updateProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/findOneByIdRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/findOneByIdResponse")
    @RequestWrapper(localName = "findOneById", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakov.tm.endpoint.FindOneById")
    @ResponseWrapper(localName = "findOneByIdResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Project findOneById(
        @WebParam(name = "session", targetNamespace = "")
                ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/findOneByIndexRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/findOneByIndexResponse")
    @RequestWrapper(localName = "findOneByIndex", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneByIndex")
    @ResponseWrapper(localName = "findOneByIndexResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Project findOneByIndex(
        @WebParam(name = "session", targetNamespace = "")
                ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/findAllRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/findAllResponse")
    @RequestWrapper(localName = "findAll", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindAll")
    @ResponseWrapper(localName = "findAllResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.malakhov.tm.endpoint.Project> findAll(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/removeOneByIdRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/removeOneByIdResponse")
    @RequestWrapper(localName = "removeOneById", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneById")
    @ResponseWrapper(localName = "removeOneByIdResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Project removeOneById(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/removeOneByIndexRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/removeOneByIndexResponse")
    @RequestWrapper(localName = "removeOneByIndex", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneByIndex")
    @ResponseWrapper(localName = "removeOneByIndexResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Project removeOneByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/clearRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/clearResponse")
    @RequestWrapper(localName = "clear", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.Clear")
    @ResponseWrapper(localName = "clearResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ClearResponse")
    public void clear(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/removeOneByNameRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/removeOneByNameResponse")
    @RequestWrapper(localName = "removeOneByName", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneByName")
    @ResponseWrapper(localName = "removeOneByNameResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Project removeOneByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/createProjectRequest", output = "http://endpoint.tm.malakhov.ru/ProjectEndpoint/createProjectResponse")
    @RequestWrapper(localName = "createProject", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.CreateProject")
    @ResponseWrapper(localName = "createProjectResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.CreateProjectResponse")
    public void createProject(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );
}
