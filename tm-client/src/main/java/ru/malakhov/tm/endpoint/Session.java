package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "session", propOrder = {
    "signature",
    "timestamp",
    "userId"
})
public class Session
    extends AbstractEntity
{

    protected String signature;
    protected Long timestamp;
    protected String userId;

    public String getSignature() {
        return signature;
    }

    public void setSignature(String value) {
        this.signature = value;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long value) {
        this.timestamp = value;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String value) {
        this.userId = value;
    }

}
