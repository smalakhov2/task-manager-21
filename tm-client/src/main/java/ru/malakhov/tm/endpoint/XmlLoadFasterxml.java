package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "xmlLoadFasterxml", propOrder = {
    "session"
})
public class XmlLoadFasterxml {

    protected Session session;

    public Session getSession() {
        return session;
    }

    public void setSession(Session value) {
        this.session = value;
    }

}
