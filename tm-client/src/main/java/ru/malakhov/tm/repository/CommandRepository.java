package ru.malakhov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.command.AbstractCommand;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        try {
            initCommands();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    private void initCommands() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.malakhov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            commandList.add(clazz.newInstance());
        }
    }

    @Override
    public @NotNull List<AbstractCommand> getCommandList() {
        return commandList;
    }

}