package ru.malakhov.tm.command.data.fasterxml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;

public final class DataXmlSaveFasterxmlCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-fasterxml-xml-save";
    }

    @Override
    public @NotNull String description() {
        return "Save xml fasterxml data.";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA FASTERXML XML SAVE]");
        serviceLocator.getDataEndpoint().xmlSaveFasterxml(session);
        System.out.println("[OK]");
    }

}
