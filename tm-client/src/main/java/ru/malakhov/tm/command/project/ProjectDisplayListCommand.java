package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Project;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;

import java.util.List;

public final class ProjectDisplayListCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-list";
    }

    @Override
    public @NotNull String description() {
        return "Display task projects.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DISPlAY PROJECTS]");
        final @NotNull List<Project> projects = serviceLocator.getProjectEndpoint().findAll(session);
        for (final @NotNull Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}
