package ru.malakhov.tm.command.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "lock-user";
    }

    @Override
    public @NotNull String description() {
        return "Lock user.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        final @Nullable String login = TerminalUtil.nextLine();
        serviceLocator.getAdminEndpoint().lockUserByLogin(session ,login);
        System.out.println("[OK]");
    }

}
