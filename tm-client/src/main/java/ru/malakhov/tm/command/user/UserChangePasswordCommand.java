package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.empty.EmptyPasswordException;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.util.TerminalUtil;

import java.util.Objects;

public final class UserChangePasswordCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "change-password";
    }

    @Override
    public @NotNull String description() {
        return "Change password.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER CURRENT PASSWORD:");
        final @Nullable String currentPassword = TerminalUtil.nextLine();

        System.out.println("ENTER NEW PASSWORD:");
        final @Nullable String newPassword = TerminalUtil.nextLine();

        serviceLocator.getUserEndpoint().changePassword(session, currentPassword , newPassword);
        System.out.println("[OK]");
    }

}
