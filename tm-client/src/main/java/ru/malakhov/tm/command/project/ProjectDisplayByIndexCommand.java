package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Project;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class ProjectDisplayByIndexCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-display-by-index";
    }

    @Override
    public @NotNull String description() {
        return "Display project by index.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final @Nullable Integer index = TerminalUtil.nextNumber() - 1;
        final @Nullable Project project = serviceLocator.getProjectEndpoint().findOneByIndex(session, index);
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

}
