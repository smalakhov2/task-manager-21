package ru.malakhov.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;

public final class LogoutCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "logout";
    }

    @Override
    public @NotNull String description() {
        return "Sign out";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getSessionEndpoint().close(session);
        serviceLocator.getSessionService().removeSession();
        System.out.println("[OK]");
    }

}
