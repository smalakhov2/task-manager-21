package ru.malakhov.tm.command.data.fasterxml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;

public final class DataJsonSaveFasterxmlCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-fasterxml-json-save";
    }

    @Override
    public @NotNull String description() {
        return "Save json fasterxml data.";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA FASTERXML JSON SAVE]");
        serviceLocator.getDataEndpoint().jsonSaveFasterxml(session);
        System.out.println("[OK]");
    }

}
