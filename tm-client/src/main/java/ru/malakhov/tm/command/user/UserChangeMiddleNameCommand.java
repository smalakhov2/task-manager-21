package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeMiddleNameCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "change-middle-name";
    }

    @Override
    public @NotNull String description() {
        return "Change middle name.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[CHANGE MIDDLE NAME]");
        System.out.println("ENTER NEW MIDDLE NAME:");
        final @Nullable String name = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().changeMiddleName(session, name);
        System.out.println("[OK]");
    }

}
