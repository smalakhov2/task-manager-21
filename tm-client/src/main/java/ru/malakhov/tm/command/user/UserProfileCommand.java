package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;

public final class UserProfileCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "profile";
    }

    @Override
    public @NotNull String description() {
        return "Profile.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[PROFILE]");
        final @NotNull String[] profile = serviceLocator.getUserEndpoint().profile(session).toArray(new String[0]);
        for (final @NotNull String s : profile) {
            System.out.println(s);
        }
    }

}
