package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-create";
    }

    @Override
    public @NotNull String description() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final @Nullable String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final @Nullable String description = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().createProject(session, name, description);
        System.out.println("[OK]");
    }

}
