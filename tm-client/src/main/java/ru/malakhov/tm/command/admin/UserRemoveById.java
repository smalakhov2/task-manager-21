package ru.malakhov.tm.command.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserRemoveById extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "remove-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Remove user by ID.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final @Nullable String id = TerminalUtil.nextLine();
        serviceLocator.getAdminEndpoint().removeById(session ,id);
        System.out.println("[OK]");
    }

}
