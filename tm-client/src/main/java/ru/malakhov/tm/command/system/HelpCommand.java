package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.List;

public final class HelpCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-h";
    }

    @Override
    public @NotNull String name() {
        return "help";
    }

    @Override
    public @NotNull String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final @NotNull List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (final @NotNull AbstractCommand command: commands) {
            System.out.println(command.name() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}
