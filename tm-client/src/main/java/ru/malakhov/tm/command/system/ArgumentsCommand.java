package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.List;

public final class ArgumentsCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-arg";
    }

    @Override
    public @NotNull String name() {
        return "arguments";
    }

    @Override
    public @NotNull String description() {
        return "Display arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final @NotNull List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (final @NotNull AbstractCommand command: commands) {
            if (command.arg() == null) continue;
            System.out.println(command.arg() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}
