package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeEmailCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "change-email";
    }

    @Override
    public @NotNull String description() {
        return "Change e-mail.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[CHANGE E-MAIL]");
        System.out.println("ENTER NEW E-MAIL:");
        final @Nullable String email = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().changeEmail(session, email);
        System.out.println("[OK]");
    }

}
