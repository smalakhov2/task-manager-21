package ru.malakhov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;

public final class DataBinaryLoadCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-binary-load";
    }

    @Override
    public @NotNull String description() {
        return "Load binary date.";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA BINARY LOAD]");
        serviceLocator.getDataEndpoint().binary64Load(session);
        System.out.println("[OK]");
    }

}
