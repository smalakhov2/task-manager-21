package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-update-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final @Nullable String id = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().findOneTaskById(session, id);
        System.out.println("ENTER NAME:");
        final @Nullable String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final @Nullable String description = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().updateTaskById(session, id, name, description);
        System.out.println("[OK]");
    }

}
