package ru.malakhov.tm.command.data.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;

public final class DataXmlLoadJaxbCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-jaxb-xml-load";
    }

    @Override
    public @NotNull String description() {
        return "Load xml jaxb date.";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA JAXB XML LOAD]");
        serviceLocator.getDataEndpoint().xmlLoadJaxb(session);
        System.out.println("[OK]");
    }

}
