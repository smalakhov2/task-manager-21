package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserCreate extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "registry";
    }

    @Override
    public @NotNull String description() {
        return "Registry";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("Enter login:");
        final @Nullable String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final @Nullable String password = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().create(login, password);
        System.out.println("[OK]");
    }

}
