package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-update-by-index";
    }

    @Override
    public @NotNull String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final @Nullable Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskEndpoint().findOneTaskByIndex(session, index);
        System.out.println("ENTER NAME:");
        final @Nullable String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final @Nullable String description = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().updateTaskByIndex(session, index, name, description);
        System.out.println("[OK]");
    }

}
