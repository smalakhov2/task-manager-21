package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-v";
    }

    @Override
    public @NotNull String name() {
        return "version";
    }

    @Override
    public @NotNull String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.2.1");
    }

}
