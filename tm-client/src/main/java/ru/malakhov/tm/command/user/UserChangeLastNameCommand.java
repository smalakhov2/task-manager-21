package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeLastNameCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "change-last-name";
    }

    @Override
    public @NotNull String description() {
        return "Change last name.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[CHANGE LAST NAME]");
        System.out.println("ENTER NEW LAST NAME:");
        final @Nullable String name = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().changeLastName(session, name);
        System.out.println("[OK]");
    }

}
