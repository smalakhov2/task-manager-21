package ru.malakhov.tm.command.data.fasterxml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;

public final class DataXmlLoadFasterxmlCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-fasterxml-xml-load";
    }

    @Override
    public @NotNull String description() {
        return "Load xml fasterxml date.";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA FASTERXML XML LOAD]");
        serviceLocator.getDataEndpoint().xmlLoadFasterxml(session);
        System.out.println("[OK]");
    }

}
