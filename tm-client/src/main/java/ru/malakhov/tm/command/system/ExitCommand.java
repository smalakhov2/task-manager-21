package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "exit";
    }

    @Override
    public @NotNull String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
