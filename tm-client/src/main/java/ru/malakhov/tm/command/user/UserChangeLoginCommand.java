package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeLoginCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "change-login";
    }

    @Override
    public @NotNull String description() {
        return "Change login.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[CHANGE LOGIN]");
        System.out.println("ENTER NEW LOGIN:");
        final @Nullable String login = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().changeLogin(session, login);
        System.out.println("[OK]");
    }

}
