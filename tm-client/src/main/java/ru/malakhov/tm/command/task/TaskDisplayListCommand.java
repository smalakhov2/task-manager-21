package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.endpoint.Task;
import ru.malakhov.tm.exception.system.AccessDeniedException;

import java.util.List;

public final class TaskDisplayListCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-list";
    }

    @Override
    public @NotNull String description() {
        return "Display task list.";
    }

    @Override
    public void execute() {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LIST TASKS]");
        final @NotNull List<Task> tasks = serviceLocator.getTaskEndpoint().findAllTask(session);
        for (final @NotNull Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}
