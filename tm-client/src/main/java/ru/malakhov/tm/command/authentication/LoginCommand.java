package ru.malakhov.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "login";
    }

    @Override
    public @NotNull String description() {
        return "sign in";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final @Nullable String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final @Nullable String password = TerminalUtil.nextLine();
        final @Nullable Session session = serviceLocator.getSessionEndpoint().open(login, password);
        serviceLocator.getSessionService().setSession(session);
        System.out.println("[OK]");
    }

}
