package ru.malakhov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.exception.system.AccessDeniedException;

public final class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-base64-clear";
    }

    @Override
    public @NotNull String description() {
        return "Clear base64 data.";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable Session session = serviceLocator.getSessionService().getSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA BASE64 CLEAR]");
        serviceLocator.getDataEndpoint().base64Clear(session);
        System.out.println("[OK]");
    }

}
