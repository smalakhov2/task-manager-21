package ru.malakhov.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.endpoint.Session;

public class SessionService implements ISessionService {

    @Nullable
    private Session session;

    @Override
    public @Nullable Session getSession() {
        return session;
    }

    @Override
    public void setSession(final @Nullable Session session) {
        this.session = session;
    }

    @Override
    public void removeSession() {
        session = null;
    }
}
