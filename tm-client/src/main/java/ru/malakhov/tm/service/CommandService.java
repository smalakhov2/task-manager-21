package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.api.service.ICommandService;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.List;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public @NotNull List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }


}