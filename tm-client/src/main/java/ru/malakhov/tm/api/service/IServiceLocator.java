package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.service.ICommandService;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    DataEndpoint getDataEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

}
