package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractCommand> getCommandList();

}
