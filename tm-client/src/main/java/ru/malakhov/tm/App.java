package ru.malakhov.tm;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.bootstrap.Bootstrap;

public final class App {

    public static void main( String[] args ) throws Exception {
        final @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
