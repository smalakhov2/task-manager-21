package ru.malakhov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.api.service.ICommandService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.*;
import ru.malakhov.tm.exception.system.UnknownArgumentException;
import ru.malakhov.tm.exception.system.UnknownCommandException;
import ru.malakhov.tm.repository.CommandRepository;
import ru.malakhov.tm.service.CommandService;
import ru.malakhov.tm.service.SessionService;
import ru.malakhov.tm.util.TerminalUtil;

import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public final class Bootstrap implements IServiceLocator {

    private @NotNull final ICommandRepository commandRepository = new CommandRepository();

    private @NotNull final ICommandService commandService = new CommandService(commandRepository);


    private @NotNull final ISessionService sessionService = new SessionService();


    private @NotNull final AdminEndpointService adminEndpointService = new AdminEndpointService();

    private @NotNull final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();


    private @NotNull final DataEndpointService dataEndpointService = new DataEndpointService();

    private @NotNull final DataEndpoint dataEndpoint = dataEndpointService.getDataEndpointPort();


    private @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    private @NotNull final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();


    private @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    private @NotNull final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();


    private @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();

    private @NotNull final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();


    private @NotNull final UserEndpointService userEndpointService = new UserEndpointService();

    private @NotNull final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();


    private @NotNull final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private @NotNull final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        initCommands(commandService.getCommandList());
    }

    private void initCommands(@NotNull List<AbstractCommand> commandList) {
        for (AbstractCommand command: commandList) init(command);
    }

    private void init(final @Nullable AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private static void displayHello() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private void process() {
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        final @NotNull String arg = args[0];
        try {
            parseArg(arg);
        } catch (Exception e) {
            logError(e);
        }
        return true;
    }

    private static void logError(@NotNull Exception e){
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    private void parseArg(@Nullable final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        final @Nullable AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        argument.execute();
    }

    private void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        final @Nullable AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    public void run(final String[] args) throws Exception {
        displayHello();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    @Override
    public @NotNull ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return sessionService;
    }

    @Override
    public @NotNull AdminEndpoint getAdminEndpoint() {
        return adminEndpoint;
    }

    @Override
    public @NotNull DataEndpoint getDataEndpoint() {
        return dataEndpoint;
    }

    @Override
    public @NotNull ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public @NotNull SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    public @NotNull TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public @NotNull UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

}